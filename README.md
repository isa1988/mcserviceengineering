# MCServiceEngineering

## Описание

Этот проект состоит из двух частей:

1. **Бэкенд**: Реализован на `mcserviceengineeringapi.ibragiimov.ru` и использует [Swagger UI](https://mcserviceengineeringapi.ibragiimov.ru/swagger/index.html) для документирования и тестирования API.
2. **Фронтенд**: Реализован на `mcserviceengineeringreact.ibragiimov.ru`.

## Бэкенд

Бэкенд часть проекта доступна по адресу [mcserviceengineeringapi.ibragiimov.ru](https://mcserviceengineeringapi.ibragiimov.ru/swagger/index.html). Здесь вы найдете Swagger UI, где можно ознакомиться с доступными эндпоинтами и протестировать их.

## Фронтенд

Фронтенд часть проекта доступна по адресу [mcserviceengineeringreact.ibragiimov.ru](https://mcserviceengineeringreact.ibragiimov.ru).

## Тестовое задание

### Задание №1

Учитывая положительное число `n > 1`, найдите разложение числа `n` на простые множители. Результатом будет строка следующего вида: "(p1**n1)(p2**n2)...(pk**nk)".

#### Пример n = 86240 результат "(2**5)(5)(7**2)(11)"


### Задание №2
Ваша задача — создать все перестановки непустой входной строки и удалить дубликаты, если они есть.

- Ввод 'a':
  - Результат: ['a']

- Ввод 'ab':
  - Результат ['ab', 'ba']

- Ввод 'abc':
  - Результат ['abc','acb','bac','bca','cab','cba']

- Ввод 'aabb':
  - Результат ['aabb', 'abab', 'abba', 'baab', 'baba', 'bbaa']


### Задание №3
Вам необходимо проверить, является ли строка ввода пользователя буквенно-цифровой. Данная строка не равна null, поэтому вам не нужно это проверять.

Строка имеет следующие условия, чтобы быть буквенно-цифровой:
- Хотя бы один символ ("" недействителен)
- Разрешенными символами являются прописные/строчные латинские буквы и цифры от 0 до 9.
- Никаких пробелов/подчеркивания

### Задание №4
Реализуйте функцию unique_in_order, которая принимает в качестве аргумента последовательность и возвращает список элементов без каких-либо элементов с одинаковым значением рядом друг с другом и сохраняет исходный порядок элементов.
#### Пример:
- uniqueInOrder("AAAABBBCCDAABBB") == {'A', 'B', 'C', 'D', 'A', 'B'}
- uniqueInOrder("ABBCcAD")         == {'A', 'B', 'C', 'c', 'A', 'D'}
- uniqueInOrder([1,2,2,3,3])       == {1,2,3}
