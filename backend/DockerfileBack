#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS base
WORKDIR /app
ENV ASPNETCORE_URLS=http://*:7080
EXPOSE 7080

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
WORKDIR /backend
COPY ["WebAPI/WebAPI.csproj", "WebAPI/"]
COPY ["MathLogice/MathLogic.csproj", "MathLogice/"]
COPY ["WebAPI/appsettings.Production.json", "WebAPI/"]
RUN dotnet restore "WebAPI/WebAPI.csproj"
COPY . .
WORKDIR "/backend/WebAPI"
RUN dotnet build "WebAPI.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "WebAPI.csproj" -c Release -o /app/publish


FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY aspnetapp.pfx .
ENV ASPNETCORE_Kestrel__Certificates__Default__Path=/app/aspnetapp.pfx
ENV ASPNETCORE_Kestrel__Certificates__Default__Password=petrel
ENV ASPNETCORE_URLS=https://+:7081;http://+:7080
ENV ASPNETCORE_HTTPS_PORT=7081
EXPOSE 7081
EXPOSE 7080
ENTRYPOINT ["dotnet", "WebAPI.dll"]
