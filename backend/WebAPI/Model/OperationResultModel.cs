﻿namespace WebAPI.Model
{
    public class OperationResultModel<T>
    {
        public string Message { get; set; }
        public ResultCode Code { get; set; }
        public T Result { get; set; }
        public bool IsSuccess
        {
            get { return Code == ResultCode.Success; }
        }
    }
}
