﻿namespace WebAPI.Model.For
{
    public class FourCollectionRequest<T>
    {
        public List<T> ItemList { get; set; } = new List<T>();
    }
}
