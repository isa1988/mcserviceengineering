﻿namespace WebAPI.Model.For
{
    public class FourResponse<T>
    {
        public List<T> ItemList { get; set; } = new List<T>();
    }
}
