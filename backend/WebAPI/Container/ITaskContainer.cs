﻿using WebAPI.Model;
using WebAPI.Model.For;
using WebAPI.Model.One;
using WebAPI.Model.Three;
using WebAPI.Model.Two;

namespace WebAPI.Container
{
    public interface ITaskContainer
    {
        Task<OperationResultModel<string>> GetResultOneTaskAsync(OneRequest request);
        Task<OperationResultModel<List<string>?>> GetResultTwoTaskAsync(TwoRequest request);
        Task<OperationResultModel<bool>> GetResultThreeTaskWithoutRegularExpressionAsync(ThreeRequest request);
        Task<OperationResultModel<bool>> GetResultThreeTaskWithRegularExpressionAsync(ThreeRequest request);
        Task<OperationResultModel<FourResponse<string>>> GetResultFourTaskStringAsync(FourStringRequest request);
        Task<OperationResultModel<FourResponse<int>>> GetResultFourTaskIntAsync(FourCollectionRequest<int> request);
    }
}
