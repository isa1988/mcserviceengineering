﻿using MathLogic;
using WebAPI.Model;
using WebAPI.Model.For;
using WebAPI.Model.One;
using WebAPI.Model.Three;
using WebAPI.Model.Two;

namespace WebAPI.Container
{
    public class TaskContainer : ITaskContainer
    {
        public async Task<OperationResultModel<string>> GetResultOneTaskAsync(OneRequest request)
        {
            try
            {
                var task = new TaskOne(request.N);
                var result = task.GetResult();
                return GetOperationResult(result, ResultCode.Success, string.Empty);
            }
            catch (ArgumentException e)
            {
                return GetOperationResult<string>(string.Empty,
                   ResultCode.FieldIsNull, e.Message);
            }
            catch ( Exception e)
            {
                return GetOperationResult<string>(string.Empty,
                   ResultCode.General, e.Message);
            }
        }

        public async Task<OperationResultModel<List<string>>> GetResultTwoTaskAsync(TwoRequest request)
        {
            try
            {
                var task = new TaskTwo(request.Input);
                var result = task.GetResult();
                return GetOperationResult(result, ResultCode.Success, string.Empty);
            }
            catch (ArgumentException e)
            {
                return GetOperationResult<List<string>>(new List<string>(),
                   ResultCode.FieldIsNull, e.Message);
            }
            catch (Exception e)
            {
                return GetOperationResult<List<string>>(new List<string>(),
                   ResultCode.General, e.Message);
            }
        }

        public async Task<OperationResultModel<bool>> GetResultThreeTaskWithRegularExpressionAsync(ThreeRequest request)
        {
            try
            {
                var task = new TaskThree(request.Input);
                var result = task.GetResult();
                return GetOperationResult(result, ResultCode.Success, string.Empty);
            }
            catch (ArgumentException e)
            {
                return GetOperationResult<bool>(false,
                   ResultCode.FieldIsNull, e.Message);
            }
            catch (Exception e)
            {
                return GetOperationResult<bool>(false,
                   ResultCode.General, e.Message);
            }
        }
        public async Task<OperationResultModel<bool>> GetResultThreeTaskWithoutRegularExpressionAsync(ThreeRequest request)
        {
            try
            {
                var task = new TaskThree(request.Input);
                var result = task.GetResultWithoutRegularExpression();
                return GetOperationResult(result, ResultCode.Success, string.Empty);
            }
            catch (ArgumentException e)
            {
                return GetOperationResult<bool>(false,
                   ResultCode.FieldIsNull, e.Message);
            }
            catch (Exception e)
            {
                return GetOperationResult<bool>(false,
                   ResultCode.General, e.Message);
            }
        }

        public async Task<OperationResultModel<FourResponse<string>>> GetResultFourTaskStringAsync(FourStringRequest request)
        {
            try
            {
                var task = new TaskFour<char>(request.Line);
                var result = new FourResponse<string>();
                var resultFromCalculation = task.GetResult();
                result.ItemList = resultFromCalculation.Select(c => c.ToString()).ToList();
                var resultToStr = result;
                return GetOperationResult(result, ResultCode.Success, string.Empty);
            }
            catch (ArgumentException e)
            {
                return GetOperationResult<FourResponse<string>>(new FourResponse<string>(),
                   ResultCode.FieldIsNull, e.Message);
            }
            catch (Exception e)
            {
                return GetOperationResult<FourResponse<string>>(new FourResponse<string>(),
                   ResultCode.General, e.Message);
            }
        }

        public async Task<OperationResultModel<FourResponse<int>>> GetResultFourTaskIntAsync(FourCollectionRequest<int> request)
        {
            try
            {
                var task = new TaskFour<int>(request.ItemList);
                var result = new FourResponse<int>();
                result.ItemList = task.GetResult();
                return GetOperationResult(result, ResultCode.Success, string.Empty);
            }
            catch (ArgumentException e)
            {
                return GetOperationResult<FourResponse<int>>(new FourResponse<int>(),
                   ResultCode.FieldIsNull, e.Message);
            }
            catch (Exception e)
            {
                return GetOperationResult<FourResponse<int>>(new FourResponse<int>(),
                   ResultCode.General, e.Message);
            }
        }



        private OperationResultModel<T> GetOperationResult<T>(T model, ResultCode code, string error)
        {
            return new OperationResultModel<T>
            {
                Result = model,
                Code = code,
                Message = error,
            };
        }
    }
}
