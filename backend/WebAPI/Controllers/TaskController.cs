﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Container;
using WebAPI.Model.For;
using WebAPI.Model.One;
using WebAPI.Model.Three;
using WebAPI.Model.Two;
using WebAPI.Model;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskContainer taskContainer;

        public TaskController(ITaskContainer taskContainer)
        {
            this.taskContainer = taskContainer;
        }

        [HttpPost("first")]
        public async Task<OperationResultModel<string>> GetResultOneTaskAsync(OneRequest request)
        {
            var result = await taskContainer.GetResultOneTaskAsync(request);
            return result;
        }

        [HttpPost("second")]
        public async Task<OperationResultModel<List<string>?>> GetResultTwoTaskAsync(TwoRequest request)
        {
            var result = await taskContainer.GetResultTwoTaskAsync(request);
            return result;
        }

        [HttpPost("thirdWithoutRegularExpression")]
        public async Task<OperationResultModel<bool>> GetResultThreeTaskWithoutRegularExpressionAsync(ThreeRequest request)
        {
            var result = await taskContainer.GetResultThreeTaskWithoutRegularExpressionAsync(request);
            return result;
        }

        [HttpPost("thirdWithRegularExpression")]
        public async Task<OperationResultModel<bool>> GetResultThreeTaskWithRegularExpressionAsync(ThreeRequest request)
        {
            var result = await taskContainer.GetResultThreeTaskWithRegularExpressionAsync(request);
            return result;
        }

        [HttpPost("fourthStr")]
        public async Task<OperationResultModel<FourResponse<string>>> GetResultFourTaskStringAsync(FourStringRequest request)
        {
            var result = await taskContainer.GetResultFourTaskStringAsync(request);
            return result;
        }

        [HttpPost("fourthIntArray")]
        public async Task<OperationResultModel<FourResponse<int>>> GetResultFourTaskIntAsync(FourCollectionRequest<int> request)
        {
            var result = await taskContainer.GetResultFourTaskIntAsync(request);
            return result;
        }
    }
}
