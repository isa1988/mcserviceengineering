﻿using System.Text.RegularExpressions;

namespace MathLogic
{
    public class TaskThree
    {
        private readonly string input;

        public TaskThree(string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentException("Строка не должна быть пустой.");
            this.input = input;
        }
        public bool GetResult()
        {
            string pattern = "^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$";
            return Regex.IsMatch(input, pattern);
        }

        public bool GetResultWithoutRegularExpression()
        {
            bool hasLetter = false;
            bool hasDigit = false;

            foreach (char c in input)
            {
                if (!char.IsLetterOrDigit(c))
                {
                    return false;
                }

                if (char.IsDigit(c))
                {
                    hasDigit = true;
                }
                else if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    hasLetter = true;
                }
                else
                {
                    return false;
                }
            }

            return hasLetter && hasDigit;
        }
    }
}
