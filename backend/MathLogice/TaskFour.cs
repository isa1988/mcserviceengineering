﻿namespace MathLogic
{
    public class TaskFour<T>
    {
        private readonly IEnumerable<T> sequence;

        public TaskFour(IEnumerable<T> sequence)
        {
            if (sequence == null || !sequence.Any())
                throw new ArgumentException("Последовательность не должна быть пустой.");
            this.sequence = sequence;
        }

        public TaskFour(string sequence)
        {
            if (string.IsNullOrWhiteSpace(sequence))
                throw new ArgumentException("Последовательность не должна быть пустой.");
            this.sequence = sequence.Cast<T>();
        }

        public List<T> GetResult()
        {
            var result = new List<T>();
            T previous = default(T);
            bool first = true;

            foreach (var element in sequence)
            {
                if (first || !EqualityComparer<T>.Default.Equals(element, previous))
                {
                    result.Add(element);
                    previous = element;
                    first = false;
                }
            }
            return result;
        }
    }
}
