﻿namespace MathLogic
{
    public class TaskTwo
    {
        private readonly string input;

        public TaskTwo(string input)
        {
            if (string.IsNullOrEmpty(input))
                throw new ArgumentException("Строка не должна быть пустой.");
            this.input = input;
        }

        public List<string> GetResult()
        {
            var permutations = GetPermutations(input);
            return permutations.Distinct().ToList();
        }

        private IEnumerable<string> GetPermutations(string str)
        {
            if (str.Length == 1)
                return new List<string> { str };

            var permutations = new List<string>();

            foreach (var c in str)
            {
                var remaining = str.Remove(str.IndexOf(c), 1);
                foreach (var permutation in GetPermutations(remaining))
                {
                    permutations.Add(c + permutation);
                }
            }

            return permutations;
        }
    }
}
