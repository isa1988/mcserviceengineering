﻿namespace MathLogic
{
    public class TaskOne
    {
        private readonly int n;

        public TaskOne(int n)
        {
            if (n <= 1)
                throw new ArgumentException("Число должно быть больше одного.");
            this.n = n;
        }

        public string GetResult()
        {
            var factors = PrimeFactors(n);
            return FormatFactors(factors);
        }

        private Dictionary<int, int> PrimeFactors(int n)
        {
            var factors = new Dictionary<int, int>();
            int i = 2;
            while (i * i <= n)
            {
                while (n % i == 0)
                {
                    if (factors.ContainsKey(i))
                    {
                        factors[i]++;
                    }
                    else
                    {
                        factors[i] = 1;
                    }
                    n /= i;
                }
                i++;
            }
            if (n > 1)
            {
                factors[n] = 1;
            }
            return factors;
        }

        private string FormatFactors(Dictionary<int, int> factors)
        {
            return string.Join("", factors.Select(f => f.Value > 1 ? $"({f.Key}**{f.Value})" : $"({f.Key})"));
        }
    }
}

