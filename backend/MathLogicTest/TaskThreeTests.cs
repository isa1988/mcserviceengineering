﻿using MathLogic;

namespace MathLogicTest
{
    public class TaskThreeTests
    {
        [TestCase("Abc123", true)]
        [TestCase("abc123", true)]
        [TestCase("abc 123", false)]
        [TestCase("abc_123", false)]
        [TestCase("V", false)]
        [TestCase("414", false)]
        [TestCase("777", false)]
        [TestCase("Bbb", false)]
        [TestCase("H5h n", false)]
        [TestCase("G5h7", true)]
        [TestCase("пр7", false)]
        [TestCase("прhy7", false)]
        public void GetResult_TestCases(string input, bool expected)
        {
            var task = new TaskThree(input);
            var result = task.GetResult();
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void GetResult_EmptyString_ThrowsArgumentException()
        {
            Assert.That(() => new TaskThree(""), Throws.TypeOf<ArgumentException>());
        }

        [TestCase("Abc123", true)]
        [TestCase("abc123", true)]
        [TestCase("abc 123", false)]
        [TestCase("abc_123", false)]
        [TestCase("V", false)]
        [TestCase("414", false)]
        [TestCase("777", false)]
        [TestCase("Bbb", false)]
        [TestCase("H5h n", false)]
        [TestCase("G5h7", true)]
        [TestCase("пр7", false)]
        [TestCase("прhy7", false)]
        public void GetResultWithoutRegularExpression_TestCases(string input, bool expected)
        {
            var task = new TaskThree(input);
            var result = task.GetResultWithoutRegularExpression();
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void GetResultWithoutRegularExpression_EmptyString_ThrowsArgumentException()
        {
            Assert.That(() => new TaskThree(""), Throws.TypeOf<ArgumentException>());
        }
    }
}
