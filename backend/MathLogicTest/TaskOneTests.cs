using MathLogic;

namespace MathLogicTest
{
    public class TaskOneTests
    {
        [Test]
        public void GetResult_ValidNumber_ReturnsCorrectFactors()
        {
            var task = new TaskOne(86240);
            var result = task.GetResult();
            Assert.That(result, Is.EqualTo("(2**5)(5)(7**2)(11)"));
        }

        [Test]
        public void GetResult_InvalidNumber_ThrowsArgumentException()
        {
            Assert.That(() => new TaskOne(1), Throws.TypeOf<ArgumentException>());
        }
    }
}