﻿using MathLogic;

namespace MathLogicTest
{
    public class TaskTwoTests
    {
        [Test]
        public void GetResult_InputA_ReturnsA()
        {
            var task = new TaskTwo("a");
            var result = task.GetResult();
            Assert.That(result, Is.EqualTo(new List<string> { "a" }));
        }

        [Test]
        public void GetResult_InputAb_ReturnsAbBa()
        {
            var task = new TaskTwo("ab");
            var result = task.GetResult();
            Assert.That(result, Is.EquivalentTo(new List<string> { "ab", "ba" }));
        }

        [Test]
        public void GetResult_InputAbc_ReturnsAllPermutations()
        {
            var task = new TaskTwo("abc");
            var result = task.GetResult();
            Assert.That(result, Is.EquivalentTo(new List<string> { "abc", "acb", "bac", "bca", "cab", "cba" }));
        }

        [Test]
        public void GetResult_InputAabb_ReturnsUniquePermutations()
        {
            var task = new TaskTwo("aabb");
            var result = task.GetResult();
            Assert.That(result, Is.EquivalentTo(new List<string> { "aabb", "abab", "abba", "baab", "baba", "bbaa" }));
        }

        [Test]
        public void GetResult_EmptyString_ThrowsArgumentException()
        {
            Assert.That(() => new TaskTwo(""), Throws.TypeOf<ArgumentException>());
        }
    }
}
