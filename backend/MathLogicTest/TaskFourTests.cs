﻿using MathLogic;

namespace MathLogicTest
{
    public class TaskFourTests
    {
        [Test]
        public void GetResult_ValidString_ReturnsUniqueElements()
        {
            var sequence = "AAAABBBCCDAABBB";
            var task = new TaskFour<char>(sequence);
            var result = task.GetResult();
            var resultToStr = result.Select(c => c.ToString());
            Assert.That(resultToStr, Is.EqualTo(new List<string> { "A", "B", "C", "D", "A", "B" }));
        }

        [Test]
        public void GetResult_ValidList_ReturnsUniqueElements()
        {
            var sequence = new List<int> { 1, 2, 2, 3, 3 };
            var task = new TaskFour<int>(sequence);
            var result = task.GetResult();
            Assert.That(result, Is.EqualTo(new List<int> { 1, 2, 3 }));
        }

        [Test]
        public void GetResult_EmptySequence_ThrowsArgumentException()
        {
            Assert.That(() => new TaskFour<int>(new List<int>()), Throws.TypeOf<ArgumentException>());
        }

        [Test]
        public void GetResult_EmptyString_ThrowsArgumentException()
        {
            Assert.That(() => new TaskFour<string>(""), Throws.TypeOf<ArgumentException>());
        }
    }
}
