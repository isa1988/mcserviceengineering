import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import MainLayout from './layouts/MainLayout';
import TaskFirst from './components/task/First/TaskFirst';
import TaskSecond from './components/task/Second/TaskSecond';
import TaskThird from './components/task/Third/TaskThird';
import TaskFour from './components/task/Fourth/TaskFourth';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={<MainLayout />}>
            <Route path="taskFirst" element={<TaskFirst />} />
            <Route path="taskSecond" element={<TaskSecond />} />
            <Route path="taskThird" element={<TaskThird />} />
            <Route path="taskFourth" element={<TaskFour />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
