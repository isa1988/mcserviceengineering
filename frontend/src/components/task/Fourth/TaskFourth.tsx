import React, { ChangeEvent, useState } from 'react';
import { ResponseModel } from '../../ResponseModel';
import GeneralFunction, {
  RequestType,
  ShowMessageType,
} from '../../GeneralFunction';
import createPNG from '../../../img/create.png';
import deletePNG from '../../../img/delete.png';

const TaskFour: React.FC = () => {
  interface TaskFourRequestStr {
    line: string;
  }

  interface TaskFourRequestArray<T> {
    itemList: T[];
  }

  interface TaskFourResponse<T> {
    itemList: T[];
  }

  const [formStr, setFormStr] = useState<TaskFourRequestStr>({ line: '' });
  const [formArray, setFormArray] = useState<TaskFourRequestArray<number>>({
    itemList: [],
  });
  const [resultStr, setResultStr] = useState<string>('');
  const [resultArrayInt, setResultArrayInt] = useState<string>('');

  const handleResponseStr = (
    response: ResponseModel<TaskFourResponse<string>>
  ) => {
    if (response.isSuccess) {
      const formattedResult = `Дано ${
        formStr.line
      } результат [ ${response.result.itemList.join(', ')} ]`;
      setResultStr((prevResult) =>
        prevResult ? `${prevResult}\n${formattedResult}` : formattedResult
      );
    }
  };

  const handleInputChangeStr = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormStr((prevFilter) => ({
      ...prevFilter,
      [name]: value,
    }));
  };

  const handleSubmitStr = async () => {
    await GeneralFunction.evenResponse(
      'api/Task/fourthStr',
      JSON.stringify(formStr),
      RequestType.POST,
      handleResponseStr,
      ShowMessageType.All
    );
  };

  const handleResponseArrayInt = (
    response: ResponseModel<TaskFourResponse<number>>
  ) => {
    if (response.isSuccess) {
      const formattedResult = `Дано [ ${formArray.itemList.join(
        ', '
      )} ] результат [] ${response.result.itemList.join(', ')} ]`;
      setResultArrayInt((prevResult) =>
        prevResult ? `${prevResult}\n${formattedResult}` : formattedResult
      );
    }
  };

  const handleInputChangeArrayInt = (
    event: ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const { value } = event.target;
    const newList = [...formArray.itemList];
    newList[index] = parseInt(value, 10) || 0;

    setFormArray({ itemList: newList });
  };

  const handleSubmitArrayInt = async () => {
    await GeneralFunction.evenResponse(
      'api/Task/fourthIntArray',
      JSON.stringify(formArray),
      RequestType.POST,
      handleResponseArrayInt,
      ShowMessageType.All
    );
  };

  const addItemArrayInt = () => {
    setFormArray((prevData) => ({
      itemList: [...prevData.itemList, 0],
    }));
  };

  // Remove item from the list
  const removeItemArrayInt = (index: number) => {
    setFormArray((prevData) => ({
      itemList: prevData.itemList.filter((_, i) => i !== index),
    }));
  };

  const uniqueInOrder = <T,>(iterable: T[]): T[] => {
    return iterable.filter((item, index) => item !== iterable[index - 1]);
  };

  const uniqueArray1 = uniqueInOrder(['A', 'B', 'C', 'D', 'A', 'B']);
  const uniqueArray2 = uniqueInOrder(['A', 'B', 'C', 'c', 'A', 'D']);
  const uniqueArray3 = uniqueInOrder([1, 2, 3]);

  return (
    <div className="task">
      <h2>Задача четвертая</h2>
      <div className="question">
        <p>
          {' '}
          Реализуйте функцию unique_in_order, которая принимает в качестве
          аргумента последовательность и возвращает список элементов без
          каких-либо элементов с одинаковым значением рядом друг с другом и
          сохраняет исходный порядок элементов.
        </p>
        <p>Пример:</p>
        <p>
          uniqueInOrder("AAAABBBCCDAABBB") == {JSON.stringify(uniqueArray1)}
        </p>
        <p>uniqueInOrder("ABBCcAD") == {JSON.stringify(uniqueArray2)}</p>
        <p>uniqueInOrder([1,2,2,3,3]) == {JSON.stringify(uniqueArray3)}</p>
      </div>
      <div className="multipleTasks">
        <div className="first">
          <div className="question">
            <p>Строковая</p>
            <label htmlFor="inputInput">Введите строку</label>
            <input
              type="text"
              name="line"
              id="inputInput"
              onChange={handleInputChangeStr}
              value={formStr.line}
              min={1}
            />
            <button onClick={handleSubmitStr}>Решить</button>
          </div>
          <div className="answer">
            <p>Ответ</p>
            {resultStr && <pre>{resultStr}</pre>}
          </div>
        </div>

        <div className="second">
          <div className="question">
            <p>С Коллекция целочисленных</p>
            <button type="button" onClick={addItemArrayInt}>
              <img
                className="createImg"
                src={createPNG}
                alt={'Добавить элемент'}
              />
            </button>
            {formArray.itemList.map((item, index) => (
              <div key={index}>
                <label htmlFor={`item-${index}`}> {index + 1}:</label>
                <input
                  type="number"
                  id={`item-${index}`}
                  value={item}
                  onChange={(e) => handleInputChangeArrayInt(e, index)}
                />
                <button type="button" onClick={() => removeItemArrayInt(index)}>
                  <img
                    className="deleteInTablle"
                    src={deletePNG}
                    alt={'Удалить'}
                  />
                </button>
              </div>
            ))}
            <button onClick={handleSubmitArrayInt}>Решить</button>
          </div>
          <div className="answer">
            <p>Ответ</p>
            {resultArrayInt && <pre>{resultArrayInt}</pre>}
          </div>
        </div>
      </div>
    </div>
  );
};

export default TaskFour;
