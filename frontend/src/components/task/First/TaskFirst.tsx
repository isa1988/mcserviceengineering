import React, { useState, ChangeEvent } from 'react';
import { ResponseModel } from '../../ResponseModel';
import GeneralFunction, {
  RequestType,
  ShowMessageType,
} from '../../GeneralFunction';

const TaskFirst: React.FC = () => {
  interface TaskFirstRequest {
    n: number;
  }

  const [formData, setFormData] = useState<TaskFirstRequest>({
    n: 1,
  });
  const [result, setResult] = useState<string>('');

  const handleResponse = (response: ResponseModel<string>) => {
    if (response.isSuccess) {
      setResult(
        (prevResult) =>
          prevResult +
          (prevResult ? '\n' : '') +
          'Дано ' +
          formData.n +
          ' ' +
          response.result
      );
    }
  };

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData((prevFilter) => ({
      ...prevFilter,
      [name]: parseInt(value),
    }));
  };

  const handleSubmit = async () => {
    await GeneralFunction.evenResponse(
      'api/Task/first',
      JSON.stringify(formData),
      RequestType.POST,
      handleResponse,
      ShowMessageType.All
    );
  };

  return (
    <div className="task">
      <h2>Задача первая</h2>
      <div className="question">
        <p>
          {' '}
          Учитывая положительное число n &gt; 1, найдите разложение числа n на
          простые множители. Результатом будет строка следующего вида:
          "(p1**n1)(p2**n2)...(pk**nk)"
        </p>
        <p> Пример: n = 86240 результат "(2**5)(5)(7**2)(11)" </p>
        <label htmlFor="nInput">Введите числа</label>
        <input
          type="number"
          name="n"
          id="nInput"
          onChange={handleInputChange}
          value={formData.n}
          min={1}
        />
        <button onClick={handleSubmit}>Решить</button>
      </div>
      <div className="answer">
        <p>Ответ</p>
        {result && <pre>{result}</pre>}
      </div>
    </div>
  );
};

export default TaskFirst;
