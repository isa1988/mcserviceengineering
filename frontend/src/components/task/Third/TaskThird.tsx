import React, { useState, ChangeEvent } from 'react';
import { ResponseModel } from '../../ResponseModel';
import GeneralFunction, {
  RequestType,
  ShowMessageType,
} from '../../GeneralFunction';

const TaskThird: React.FC = () => {
  interface TaskThirdRequest {
    input: string;
  }
  const [formWithoutRegularExpression, setFormWithoutRegularExpression] =
    useState<TaskThirdRequest>({ input: '' });
  const [formWithRegularExpression, setFormWithRegularExpression] =
    useState<TaskThirdRequest>({ input: '' });
  const [resultWithoutRegularExpression, setResultWithoutRegularExpression] =
    useState<string>('');
  const [resultWithRegularExpression, setResultWithRegularExpression] =
    useState<string>('');

  const handleResponseWithoutRegularExpression = (
    response: ResponseModel<boolean>
  ) => {
    if (response.isSuccess) {
      setResultWithoutRegularExpression(
        (prevResult) =>
          prevResult +
          (prevResult ? '\n' : '') +
          'Дано ' +
          formWithoutRegularExpression.input +
          ' ' +
          (response.result ? 'Валидный' : 'Невалидный')
      );
    }
  };

  const handleInputChangeWithoutRegularExpression = (
    e: ChangeEvent<HTMLInputElement>
  ) => {
    const { name, value } = e.target;
    setFormWithoutRegularExpression((prevFilter) => ({
      ...prevFilter,
      [name]: value,
    }));
  };

  const handleSubmitWithoutRegularExpression = async () => {
    await GeneralFunction.evenResponse(
      'api/Task/thirdWithoutRegularExpression',
      JSON.stringify(formWithoutRegularExpression),
      RequestType.POST,
      handleResponseWithoutRegularExpression,
      ShowMessageType.All
    );
  };

  const handleResponseWithRegularExpression = (
    response: ResponseModel<boolean>
  ) => {
    if (response.isSuccess) {
      setResultWithRegularExpression(
        (prevResult) =>
          prevResult +
          (prevResult ? '\n' : '') +
          'Дано ' +
          formWithRegularExpression.input +
          ' ' +
          (response.result ? 'Валидный' : 'Невалидный')
      );
    }
  };

  const handleInputChangeWithRegularExpression = (
    e: ChangeEvent<HTMLInputElement>
  ) => {
    const { name, value } = e.target;
    setFormWithRegularExpression((prevFilter) => ({
      ...prevFilter,
      [name]: value,
    }));
  };

  const handleSubmitWithRegularExpression = async () => {
    await GeneralFunction.evenResponse(
      'api/Task/thirdWithRegularExpression',
      JSON.stringify(formWithRegularExpression),
      RequestType.POST,
      handleResponseWithRegularExpression,
      ShowMessageType.All
    );
  };

  return (
    <div className="task">
      <h2>Задача третья</h2>
      <div className="question">
        <p>
          Вам необходимо проверить, является ли строка ввода пользователя
          буквенно-цифровой. Данная строка не равна null, поэтому вам не нужно
          это проверять.
        </p>
        <p>Строка имеет следующие условия, чтобы быть буквенно-цифровой:</p>
        <p>• Хотя бы один символ "" недействителен</p>
        <p>
          • Разрешенными символами являются прописные/строчные латинские буквы и
          цифры от 0 до 9.
        </p>
        <p>• Никаких пробелов/подчеркивания</p>
      </div>
      <div className="multipleTasks">
        <div className="first">
          <div className="question">
            <p>Без использования регулярных выражений</p>
            <label htmlFor="inputInput">Введите строку</label>
            <input
              type="text"
              name="input"
              id="inputInput"
              onChange={handleInputChangeWithoutRegularExpression}
              value={formWithoutRegularExpression.input}
              min={1}
            />
            <button onClick={handleSubmitWithoutRegularExpression}>
              Решить
            </button>
          </div>
          <div className="answer">
            <p>Ответ</p>
            {resultWithoutRegularExpression && (
              <pre>{resultWithoutRegularExpression}</pre>
            )}
          </div>
        </div>

        <div className="second">
          <div className="question">
            <p>С использованием регулярных выражений</p>
            <label htmlFor="inputInput">Введите строку</label>
            <input
              type="text"
              name="input"
              id="inputInput"
              onChange={handleInputChangeWithRegularExpression}
              value={formWithRegularExpression.input}
              min={1}
            />
            <button onClick={handleSubmitWithRegularExpression}>Решить</button>
          </div>
          <div className="answer">
            <p>Ответ</p>
            {resultWithRegularExpression && (
              <pre>{resultWithRegularExpression}</pre>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default TaskThird;
