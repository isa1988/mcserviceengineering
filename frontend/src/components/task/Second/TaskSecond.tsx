import React, { ChangeEvent, useState } from 'react';
import GeneralFunction, {
  RequestType,
  ShowMessageType,
} from '../../GeneralFunction';
import { ResponseModel } from '../../ResponseModel';

const TaskSecond: React.FC = () => {
  interface TaskSecondRequest {
    input: string;
  }
  const [formData, setFormData] = useState<TaskSecondRequest>({
    input: '',
  });
  const [result, setResult] = useState<string>('');

  const handleResponse = (response: ResponseModel<string[]>) => {
    if (response.isSuccess) {
      setResult((prevResult) => {
        const formattedResult = `Дано ${
          formData.input
        } { ${response.result.join(', ')} }`;
        return prevResult
          ? prevResult + '\n' + formattedResult
          : formattedResult;
      });
    }
  };

  const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormData((prevFilter) => ({
      ...prevFilter,
      [name]: value,
    }));
  };

  const handleSubmit = async () => {
    await GeneralFunction.evenResponse(
      'api/Task/second',
      JSON.stringify(formData),
      RequestType.POST,
      handleResponse,
      ShowMessageType.All
    );
  };

  return (
    <div className="task">
      <h2>Задача вторая</h2>
      <div className="question">
        <p>
          {' '}
          Ваша задача — создать все перестановки непустой входной строки и
          удалить дубликаты, если они есть.
        </p>
        <p>Пример:</p>
        <p>Ввод 'a':</p>
        <p>Результат: ['a']</p>

        <p>Ввод 'ab':</p>
        <p>Результат ['ab', 'ba']</p>

        <p>Ввод 'abc':</p>
        <p>Результат ['abc','acb','bac','bca','cab','cba']</p>

        <p>Ввод 'aabb':</p>
        <p>Результат ['aabb', 'abab', 'abba', 'baab', 'baba', 'bbaa']</p>

        <label htmlFor="inputInput">Введите строку</label>
        <input
          type="text"
          name="input"
          id="inputInput"
          onChange={handleInputChange}
          value={formData.input}
          min={1}
        />
        <button onClick={handleSubmit}>Решить</button>
      </div>
      <div className="answer">
        <p>Ответ</p>
        {result && <pre>{result}</pre>}
      </div>
    </div>
  );
};

export default TaskSecond;
