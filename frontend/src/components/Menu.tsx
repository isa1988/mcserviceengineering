import React from 'react';
import { Link } from 'react-router-dom';

const Menu: React.FC = () => {
  return (
    <nav>
      <Link to="taskFirst">Задача №1</Link>
      <Link to="taskSecond">Задача №2</Link>
      <Link to="taskThird">Задача №3</Link>
      <Link to="taskFourth">Задача №4</Link>
    </nav>
  );
};

export default Menu;
