export interface ResponseModel<T> {
  message: string;
  code: ResultCode;
  result: T;
  isSuccess: boolean;
}

export enum ResultCode {
  Success = 0,
  LoginError = 1,
  FieldIsNull = 2,
  LineIsNotSearch = 3,
  EqualInDb = 4,
  DoesNotMatch = 5,
  General = 6,
  Information = 7,
}
