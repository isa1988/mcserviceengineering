import React from 'react';
import ReactDOM from 'react-dom/client';
import axios, { AxiosResponse, AxiosError } from 'axios';
import PopupComponent, { ResultCode } from './PopupComponentProps';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSort,
  faSortUp,
  faSortDown,
} from '@fortawesome/free-solid-svg-icons';
import devConfig from '../devConfig';
import prodConfig from '../prodConfig';

export enum RequestType {
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  DELETE = 'delete',
}

export const evenResponse = async (
  url: string,
  requestData: any,
  requestType: RequestType,
  successFun: (data: any) => void,
  show: ShowMessageType
): Promise<number> => {
  let responseCode: number;
  let message: string;
  let messageType: ResultCode;
  try {
    messageType = ResultCode.Success;
    message = '';

    const config =
      process.env.NODE_ENV === 'production' ? prodConfig : devConfig;
    url = config.apiUrl + url;
    let response: AxiosResponse<any> | undefined;

    if (requestType === RequestType.GET) {
      response = await axios.get(url);
    } else if (requestType === RequestType.POST) {
      response = await axios.post(url, requestData, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
    } else if (requestType === RequestType.PUT) {
      response = await axios.put(url, requestData);
    } else if (requestType === RequestType.DELETE) {
      response = await axios.delete(url);
    }

    if (response) {
      if (response.data.isSuccess) {
        successFun(response.data);
        message = 'Операция прошла успеша';
        messageType = response.data.code;
        responseCode = 200;
      } else {
        successFun(response.data);
        message = response.data.message;
        messageType = response.data.code;
        responseCode = 200;
      }
    } else {
      messageType = ResultCode.LoginError;
      responseCode = 400;
    }
  } catch (error: unknown) {
    const err = error as AxiosError;
    message = err.message || 'Unknown error';
    messageType = ResultCode.LoginError;

    responseCode = 400;
    console.error(error);
  }

  if (show == ShowMessageType.All || show == ShowMessageType.OnlyError) {
    let container = document.getElementById('popupNotification') as HTMLElement;
    const toastContainer = ReactDOM.createRoot(container);
    toastContainer.render(
      <React.StrictMode>
        <PopupComponent message={message} messageType={messageType} />
      </React.StrictMode>
    );
  }
  return responseCode;
};

export enum ShowMessageType {
  None,
  All,
  OnlyError,
}

export enum OrderType {
  ASC,
  DESC,
}

export enum Deleted {
  UnDeleted = 0,
  Deleted = 1,
}

export interface QueryParams<T, TOrder> {
  filter: T;
  order: OrderPagging<TOrder>;
}

export interface OrderPagging<TOrder> {
  sizeOfPage: number;
  field: TOrder;
  orderDirection: OrderType;
  currentPage: number;
}

export interface PeriodFilter<T> {
  start?: T | null;
  end?: T | null;
}

export const renderSortIcon = <T,>(columnName: T, order: OrderPagging<T>) => {
  if (order.field === columnName) {
    return order.orderDirection === OrderType.DESC ? (
      <FontAwesomeIcon icon={faSortUp} />
    ) : (
      <FontAwesomeIcon icon={faSortDown} />
    );
  } else {
    return <FontAwesomeIcon icon={faSort} />;
  }
};

const handleOrderFieldChangeSetQueryParams = <T, TOrder>(
  newOrder: OrderPagging<TOrder>,
  setQueryParams: React.Dispatch<React.SetStateAction<QueryParams<T, TOrder>>>
) => {
  setQueryParams((prevQueryParams) => ({
    ...prevQueryParams,
    order: {
      ...prevQueryParams.order,
      ...newOrder,
    },
  }));
};

const handleOrderFieldChange = <T, TOrder>(
  selected: TOrder,
  queryParams: QueryParams<T, TOrder>,
  setQueryParams: React.Dispatch<React.SetStateAction<QueryParams<T, TOrder>>>
) => {
  if (!selected) return;

  if (queryParams.order.field !== selected) {
    handleOrderFieldChangeSetQueryParams<T, TOrder>(
      {
        sizeOfPage: queryParams.order.sizeOfPage,
        field: selected,
        orderDirection: 0,
        currentPage: 1,
      },
      setQueryParams
    );
  } else {
    handleOrderFieldChangeSetQueryParams<T, TOrder>(
      {
        currentPage: queryParams.order.currentPage,
        field: queryParams.order.field,
        sizeOfPage: queryParams.order.sizeOfPage,
        orderDirection: queryParams.order.orderDirection === 0 ? 1 : 0,
      },
      setQueryParams
    );
  }
};

const GeneralFunction = {
  evenResponse,
  renderSortIcon,
  handleOrderFieldChange,
};

export default GeneralFunction;
