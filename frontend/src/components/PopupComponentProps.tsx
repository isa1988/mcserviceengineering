import React, { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export enum ResultCode {
  Success = 0,
  LoginError = 1,
  FieldIsNull = 2,
  LineIsNotSearch = 3,
  EqualInDb = 4,
  DoesNotMatch = 5,
  General = 6,
  Information = 7,
}

interface PopupComponentProps {
  message: string;
  messageType: ResultCode;
}

const PopupComponent: React.FC<PopupComponentProps> = ({
  message = '',
  messageType = ResultCode.LoginError,
}) => {
  useEffect(() => {
    switch (messageType) {
      case ResultCode.Success:
        showSuccess();
        break;
      case ResultCode.LoginError:
      case ResultCode.FieldIsNull:
      case ResultCode.LineIsNotSearch:
      case ResultCode.EqualInDb:
      case ResultCode.DoesNotMatch:
      case ResultCode.General:
        showError();
        break;
      case ResultCode.Information:
        showInfo();
        break;
      default:
        break;
    }
  }, [message, messageType]);

  const showSuccess = () => {
    toast.success(message, {
      position: 'top-right',
      autoClose: 10000, // Close after 10 seconds
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  const showError = () => {
    toast.error(message, {
      position: 'top-right',
      autoClose: 10000, // Close after 10 seconds
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  const showInfo = () => {
    toast.info(message, {
      position: 'top-right',
      autoClose: 10000, // Close after 10 seconds
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  return (
    <div>
      <ToastContainer autoClose={10000} />
    </div>
  );
};

export default PopupComponent;
